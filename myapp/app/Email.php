<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Email extends Model
{
    protected $fillable = ['name','number', 'email', 'batch_id'];

    
    public function batch()
    {
        return $this->belongsTo(Batch::class);
    }
}
