<?php

namespace App\Imports;

use App\Email;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class EmailsImport implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */

    public function model(array $row)
    {
        return new Email([
            //
            'name'          => $row['name'],
            'number'        => $row['number'],
            'email'         => $row['email'],
            'batch_id'      => request()->get('batchid')
        ]);
    }
}
