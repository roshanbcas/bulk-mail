<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Batch extends Model
{
    public function list()
    {
        $batches = Batch::all();
        return view('batches', compact('batches'));
    }

    //foreign key relation
    public function emails()
    {
        return $this->hasMany(Email::class);
    }

    public function batch()
    {
        return $this->belongsTo(User::class);
    }
}
