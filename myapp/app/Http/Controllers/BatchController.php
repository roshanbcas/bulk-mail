<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Batch;

class BatchController extends Controller
{
    //

    public function store()
    {
        // validation
        $data = request()->validate([
            'name' => 'required|min:4'
        ]);

        $batch = new Batch();
        $batch->name = request('name');
        $batch->user_id = auth()->user()->id;
        $batch->save();

        return back();
    }
}
