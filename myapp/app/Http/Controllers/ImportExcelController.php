<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Exports\EmailsExport;
use App\Imports\EmailsImport;
use Maatwebsite\Excel\Facades\Excel;

class ImportExcelController extends Controller
{
    
    public function importExportView()
    {
        return view('import');
    }

    public function export()
    {
        return Excel::download(new EmailsExport, 'emails.xlsx');
    }

    public function import()
    {
        Excel::import(new EmailsImport, request()->file('select_file'));

        return back();
    }
}
