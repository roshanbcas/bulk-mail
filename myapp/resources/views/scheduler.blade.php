@extends('layouts.master')

<!-- Side Menu Section -->
@section('side-menu')
<li class="nav-item">
    <a href="{{ asset('/home') }}" class="nav-link">
        <i class="fas fa-mail-bulk"></i>
        <p>
        Email Manager
        </p>
    </a>
</li>
<li class="nav-item">
    <a href="{{ asset('/scheduler') }}" class="nav-link active">
        <i class="fas fa-envelope-open-text"></i>
        <p>
        Email Scheduler 
        </p>
    </a>
</li>
@endsection

<!-- Content Section -->
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Email Scheduler</h1>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-9">
            <div class="card card-primary card-outline">
              <div class="card-header">
                <h3 class="card-title">Compose New Message</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <div class="form-group">
                  <input class="form-control" placeholder="To:">
                </div>
                <div class="form-group">
                  <input class="form-control" placeholder="Subject:">
                </div>
                <div class="form-group">
                    <textarea id="composenote" name="composenote" class="form-control" placeholder="Type your email here."></textarea>
                </div>
              </div>
              <!-- /.card-body -->
              <div class="card-footer">
                <div class="float-right">
                  <button type="submit" class="btn btn-primary"><i class="fa fa-envelope"></i> Send</button>
                </div>
              </div>
              <!-- /.card-footer -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
</div>
@endsection
