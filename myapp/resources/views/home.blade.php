@extends('layouts.master')

<!-- Side Menu Section -->
@section('side-menu')
<li class="nav-item">
    <a href="{{ asset('/home') }}" class="nav-link active">
        <i class="fas fa-mail-bulk"></i>
        <p>
        Email Manager
        </p>
    </a>
</li>
<li class="nav-item">
    <a href="{{ asset('/scheduler') }}" class="nav-link">
        <i class="fas fa-envelope-open-text"></i>
        <p>
        Email Scheduler 
        </p>
    </a>
</li>
@endsection

<!-- Content Section -->
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Email Manager</h1>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-6">
            <div class="card">
              <div class="card-body">
                <h5 class="card-title">Download Template</h5>
                <p class="card-text">
                  Click below button to download the template.
                </p>

                <a href="/templates/template.xlsx" class="card-link btn bg-gradient-success btn-flat btn-sm">Download Template</a>
              </div>
            </div>
          </div>
          <div class="col-lg-6">
          <div class="card card-primary card-outline">
            <div class="card-header">
                <h3 class="card-title">Upload email collection</h3>
            </div>
              <!-- /.card-header -->
            <!-- form start -->
            <form action="{{ route('import') }}" method="POST" enctype="multipart/form-data">
              @csrf
              <div class="card-body">                  
              <div class="form-group">
                <label>Collections</label>
                <select name="batchid" id="batchid" class="form-control select">
                  @if(Auth::user()->batches->count())
                    @foreach (Auth::user()->batches as $batch)
                      <option value="{{ $batch->id }}"> {{ $batch->name }}</option>
                    @endforeach
                  @else 
                    <option value="0">Add Batches</option>
                  @endif
                </select>
              </div>
              <div class="form-group">
                <label for="file">File input</label>
                <div class="input-group">
                  <div class="custom-file">
                    <input type="file" class="custom-file-input" id="select_file" name="select_file">
                    <label class="custom-file-label" id="select-file-label" for="file">Choose file</label>
                  </div>
                </div>
              </div>
              </div>
              @if(count($errors) > 0)
                  <div class="alert alert-danger">
                    Upload Validation Error<br><br>
                    <ul>
                      @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>    
                      @endforeach
                    </ul>
                  </div>
              @endif

              @if($message = Session::get('success'))
                <div class="alert alert-success alert-block">
                  <button type="button" class="close" data-dismiss="alert">x</button>
                  <strong>{{ $message }}</strong>
                </div>
              @endif
              <!-- /.card-body -->
              <div class="card-footer">
                <button type="submit" class="btn btn-primary">Upload</button>
                </form>
                <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal-add">
                  Add New Batch
                </button>
              </div>
            </div>
            <!-- /.card -->

            <div class="modal fade" id="modal-add">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <h4 class="modal-title">Add Collection</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <form action="batches" method="POST" role="form">
                    <div class="form-gruop">
                      <label for="batch-name">Batch Name</label>
                      <input type="text" id="name" name="name" class="form-control form-control-md" placeholder="Collection Name" required>  
                    </div>
                  </div>
                  <div class="modal-footer justify-content-between">
                    <button type="submit" class="btn btn-primary">Add</button>
                    @csrf
                    </form>
                  </div>
                </div>
                <!-- /.modal-content -->
              </div>
              <!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->

          </div>
          <!-- /.col-md-6 -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
</div>
@endsection
